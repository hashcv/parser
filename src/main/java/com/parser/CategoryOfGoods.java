/**
 * 
 */
package com.parser;

/**
 * @author gggg
 *
 */
public class CategoryOfGoods {
	private String Name;
	private String URL;
	private SuperMarket Sm;

	/**
	 * @return the name
	 */
	public String getName() {
		return Name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		Name = name;
	}

	/**
	 * @return the uRL
	 */
	public String getURL() {
		return URL;
	}

	/**
	 * @param uRL
	 *            the uRL to set
	 */
	public void setURL(String uRL) {
		URL = uRL;
	}

	/**
	 * @return the sm
	 */
	public SuperMarket getSm() {
		return Sm;
	}

	/**
	 * @param sm
	 *            the sm to set
	 */
	public void setSm(SuperMarket sm) {
		Sm = sm;
	}
}
