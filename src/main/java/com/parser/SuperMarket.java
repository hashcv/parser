/**
 * 
 */
package com.parser;

/**
 * @author gggg
 *
 */
public class SuperMarket {
	private String Name;
	private String URL;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	/**
	 * @return the uRL
	 */
	public String getURL() {
		return URL;
	}

	/**
	 * @param uRL
	 *            the uRL to set
	 */
	public void setURL(String uRL) {
		URL = uRL;
	}

}
