package com.parser;

import java.io.IOException;
import java.util.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

//import com.parser.*;

public class HTMLParserExample {

	public static void main(String[] args) {

		Document doc;
		ArrayList<SuperMarket> sm = new ArrayList<SuperMarket>();
		ArrayList<CategoryOfGoods> cats = new ArrayList<CategoryOfGoods>();

		try {

			// need http protocol
			doc = Jsoup
					.connect("http://mysupermarket.org.ua/index.php")
					.ignoreContentType(true)
					.userAgent(
							"Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
					.referrer("http://www.google.com").timeout(12000)
					.followRedirects(true).get();

			Elements links = doc.select("a[href~="
					+ java.util.regex.Pattern.quote("index.php?myr_shop=")
					+ "]");
			for (Element link : links) {
				if (link.attr("href").indexOf("&") == -1) {
					SuperMarket market = new SuperMarket();
					market.setName(link.text());
					market.setURL(link.attr("href"));
					sm.add(market);
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		/*
		 * for (int i = 0; i < sm.size(); i++) {
		 * System.out.println(sm.get(i).getName() + " " + sm.get(i).getURL()); }
		 */

		for (SuperMarket k : sm) {

			try {

				doc = Jsoup
						.connect("http://mysupermarket.org.ua/" + k.getURL())
						.ignoreContentType(true)
						.userAgent(
								"Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
						.referrer("http://www.google.com").timeout(12000)
						.followRedirects(true).get();

				Elements links = doc.select("a[href~="
						+ java.util.regex.Pattern.quote(k.getURL() + "&cat=")
						+ "]");
				for (Element link : links) {
					// if (link.attr("href").indexOf("&") == -1) {
					CategoryOfGoods cat = new CategoryOfGoods();
					cat.setName(link.text());
					cat.setURL(link.attr("href"));
					cat.setSm(k);
					cats.add(cat);

				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			for (CategoryOfGoods cc : cats) {
				System.out.println(cc.getSm().getName()+"  "+cc.getName()+ "   "+cc.getURL());
			}
		}
	}

}
